//
//  TouchDrawView.h
//  CaplessCoderPaint
//


#import <UIKit/UIKit.h>
#import "Line.h"

@interface TouchDrawView : UIView {
    BOOL isCleared;
}

@property (nonatomic) Line *currentLine;
@property (nonatomic) NSMutableArray *linesCompleted;
@property (nonatomic) UIColor *drawColor;

- (void)undo;
- (void)redo;
@end
