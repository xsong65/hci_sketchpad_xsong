//
//  Common.h
//  CaplessCoderPaint
//


#import <Foundation/Foundation.h>

@interface Common : NSObject

+ (BOOL)color:(UIColor *)color1
isEqualToColor:(UIColor *)color2
withTolerance:(CGFloat)tolerance;

@end
