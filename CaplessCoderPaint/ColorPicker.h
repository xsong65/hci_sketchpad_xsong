//
//  ColorPicker.h
//  CaplessCoderPaint
//


#import <UIKit/UIKit.h>

@protocol ColorPickerDelegate <NSObject>
@optional
- (void)aColorPickerIsSelected:(UIColor *)color;
@end

@interface ColorPicker : UIView

@property (nonatomic,retain) id <ColorPickerDelegate> delegate;

@end
